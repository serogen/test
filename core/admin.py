from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django import forms
from django.db import models
from core.models import (
    Key,
    Record,
)


class KeyAdmin(admin.ModelAdmin):
    fields = [
        'key',
        'status',
    ]
    list_display = [
        'id',
        'key',
        'create_time',
        'status',

    ]
    search_fields = [
        'key',
    ]
    list_filter = (
        ('status'),
    )
admin.site.register(Key, KeyAdmin)



class RecordAdmin(admin.ModelAdmin):
    fields = [
        'file',
        'type',
        'user',
    ]
    list_display = [
        'id',
        'user',
        'file',
        'type',

    ]
    list_filter = (
        ('type'),
    )
admin.site.register(Record, RecordAdmin)


