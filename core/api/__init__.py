#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import routers
from core.api.viewsets import (
    KeyViewSet,
    RecordViewSet,
    
)

router = routers.DefaultRouter()

router.register(
    r'key',
    KeyViewSet,
    base_name='key'
)

router.register(
    r'record',
    RecordViewSet,
    base_name='record'
)

