#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied
from django.db.models import Q
from collections import OrderedDict
from core.models import (
    Key,
    Record,
)

class KeySerializer(serializers.ModelSerializer):
    status_verbose = serializers.SerializerMethodField()
    def get_status_verbose(self, obj):
        return Key.STATUS_LIST[obj.status][1]

    class Meta:
        model = Key
        fields = (
            'id',
            'key',
            'create_time',
            'status',
            'status_verbose',
        )
        read_only_fields = ('key', 'create_time')

class RecordSerializer(serializers.ModelSerializer):
    type_verbose = serializers.SerializerMethodField()
    def get_type_verbose(self, obj):
        return Record.TYPE_LIST[obj.type][1]

    class Meta:
        model = Record
        fields = (
            'id',
            'user',
            'type',
            'file',
            'type_verbose',
        )

