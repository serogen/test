# coding=utf-8

from django.contrib.auth.models import User
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import (
    TokenAuthentication,
    SessionAuthentication,
    BasicAuthentication,
)
from rest_framework.permissions import IsAuthenticated

import pprint
import logging

logger = logging.getLogger(__name__)
pprint = pprint.PrettyPrinter(indent=4).pprint


class JSONView(APIView):

    renderer_classes = (
        JSONRenderer,
    )
    """
    authentication_classes = (
        TokenAuthentication,
        SessionAuthentication,
        BasicAuthentication,
    )
    permission_classes = (
        IsAuthenticated,
    )
    """

    def response(self, data):
        return Response(data)

