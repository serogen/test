#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from .base import JSONView

from rest_framework.decorators import api_view
from django.db.models import Max, Count
from core.models import (
    Key,
)
from tst.settings import *

import logging
import pprint
pprint = pprint.PrettyPrinter(indent=4).pprint
logger = logging.getLogger('all_to_file')

def next_key(key="", add=1):
    max_digit = len(CHARS)

    # строковый ключ в массив
    lst = []
    for c in key:
        lst.append(CHARS.find(c))

    # прибавляем число в СС с основанием, равным длине массива возможных символов
    lst2 = []
    for e in reversed(lst):
        lst2.append((e + add) % max_digit)
        add = (e + add) // max_digit
    if add > 0:
        lst2.append(add)

    # делаем обратно строку
    ans = ""
    for e in reversed(lst2):
        ans = ans + CHARS[e]
    return ans

def max_keys_count():
    n = KEY_LEN
    max_digit = len(CHARS)
    ans = 0
    while n > 0:
        ans += max_digit ** n
        n -= 1
    return ans - 1

class CreateKeyView(JSONView):
    def get(self, request):
        try:
            last_key = Key.objects.latest('key').key
            new_key = next_key(last_key)
            if len(new_key) > KEY_LEN:
                return self.response({'status': 'ERROR', 'message': 'Max key reached'})

        except:
            # no keys in db
            new_key = '0000'

        key = Key(
            key=new_key
        )
        key.save()

        return self.response({'status': 'OK', 'key': new_key})

class UseKeyView(JSONView):
    def get(self, request):
        find_key = request.GET.get('key')
        try:
            key = Key.objects.get(key=find_key)
            if key.status == Key.VALID:
                key.status = Key.USED
                key.save()
            else:
                return self.response({'status': 'ERROR', 'message': 'Cant use used key'})
        except:
            # not found
            return self.response({'status': 'ERROR', 'message': 'Key not found'})

        return self.response({'status': 'OK'})

class CheckKeyView(JSONView):
    def get(self, request):
        find_key = request.GET.get('key')
        try:
            key = Key.objects.get(key=find_key)

        except:
            # not found
            return self.response({'status': 'ERROR', 'message': 'Key not found'})

        return self.response({'status': 'OK', 'key_status': Key.STATUS_LIST[key.status][1]})

class SummaryView(JSONView):
    def get(self, request):
        have_keys = Key.objects.all().count()
        max_keys = max_keys_count()

        return self.response({
            'status': 'OK',
            'max_keys': max_keys,
            'have_keys': have_keys,
            'keys_left': max_keys - have_keys,
        })


