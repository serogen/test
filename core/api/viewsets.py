#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import viewsets, filters
from rest_framework.exceptions import (
    PermissionDenied,
    ValidationError,
)
import django_filters
from rest_framework.response import Response

from core.models import (
    Key,
    Record,
)

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from collections import defaultdict
from core.api.serializers import (
    KeySerializer,
    RecordSerializer,
)
from datetime import datetime
from rest_framework.renderers import JSONRenderer

import logging
logger = logging.getLogger(__name__)


class KeyViewSet(viewsets.ModelViewSet):
    model = Key
    queryset = Key.objects.all()
    serializer_class = KeySerializer
    #permission_classes = (IsAuthenticated,)
    filter_fields = ('status',)
    ordering_fields = ('id',)

    #def get_queryset(self):
    #    user = self.request.user


class RecordViewSet(viewsets.ModelViewSet):
    model = Record
    queryset = Record.objects.all()
    serializer_class = RecordSerializer
    #permission_classes = (IsAuthenticated,)
    filter_fields = ('type',)
    ordering_fields = ('id',)


