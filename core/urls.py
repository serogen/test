from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView
from core import views
from core.api import router

from core.api.views import (
    CreateKeyView,
    UseKeyView,
    CheckKeyView,
    SummaryView,
)


#index_view = TemplateView.as_view(template_name='core/index.html')

urlpatterns = [
    #url(r'^$', index_view, name='core-index'),
    url(r'^api/', include(router.urls)),

    url(r'^api/create_key/$', CreateKeyView.as_view()),
    url(r'^api/use_key/$', UseKeyView.as_view()),
    url(r'^api/check_key/$', CheckKeyView.as_view()),
    url(r'^api/summary/$', SummaryView.as_view()),

]

