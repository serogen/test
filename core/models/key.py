# coding=utf-8

from __future__ import unicode_literals
import os
import json
from django.db import models, transaction
from datetime import datetime, date
from django.conf import settings
from django.utils import timezone
from tst.settings import *

import logging
logger = logging.getLogger(__name__)

null = {'null': True, 'blank': True, }


# данные, загружаемые в систему, полученные в результате проведения эксперимента
class Key(models.Model):

    key = models.CharField(
        max_length=4,
        null=False,
        blank=False,
        unique=True,
        verbose_name="Key",
    )
    create_time = models.DateTimeField(
        auto_now_add=True,
        editable=True,
        verbose_name="Create date",
    )

    (VALID, USED) = (0, 1,)
    STATUS_LIST = (
        (VALID, 'Valid'),
        (USED, 'Used'),
    )
    status = models.IntegerField(
        choices=STATUS_LIST,
        default=VALID,
        verbose_name="Status",
    )


    def __str__(self):
        return "%s. %s. %s" % (
            self.id,
            self.key,
            self.STATUS_LIST[self.status][1]
        )


    class Meta:
        verbose_name_plural = 'Keys'
        verbose_name = 'Key'


