# coding=utf-8

from __future__ import unicode_literals
import os
import json
from django.db import models, transaction
from datetime import datetime, date
from django.conf import settings
from django.utils import timezone
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User
from tst.settings import *

import logging
logger = logging.getLogger(__name__)

fs = FileSystemStorage(location=MEDIA_FOLDER)
def get_record_path(instance, filename):
    url = "records/%s" % (filename)
    return os.path.join(url)


# данные, загружаемые в систему, полученные в результате проведения эксперимента
class Record(models.Model):

    user = models.ForeignKey(
        User,
        related_name='user',
        null=True,
        blank=True,
        verbose_name='Пользователь'
    )

    file = models.FileField(
        upload_to=get_record_path,
        storage=fs,
        null=True,
        blank=True,
        verbose_name="record file",
    )

    create_time = models.DateTimeField(
        auto_now_add=True,
        editable=True,
        verbose_name="Create date",
    )

    (AM, FU) = (1, 0,)
    TYPE_LIST = (
        (AM, 'AM'),
        (FU, 'FU'),
    )
    type = models.IntegerField(
        choices=TYPE_LIST,
        default=FU,
        verbose_name="Type",
    )


    def __str__(self):
        return "%s. %s. %s. %s" % (
            self.id,
            self.TYPE_LIST[self.type][1],
            self.user,
            self.file.name
        )


    class Meta:
        verbose_name_plural = 'Records'
        verbose_name = 'Record'


