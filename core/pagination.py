# coding=utf-8

from rest_framework.pagination import PageNumberPagination
from tst.settings import PAGINATION_PAGE_SIZE

class Pagination(PageNumberPagination):
    page_size = PAGINATION_PAGE_SIZE
    page_size_query_param = 'limit'
    max_page_size = 1000