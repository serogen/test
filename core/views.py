import requests
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404, HttpResponseBadRequest
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView
from django.views.generic.base import View
from rest_framework import views

from tst.settings import *
