git clone / git pull
cd tst

# https://docs.docker.com/install/linux/docker-ce/ubuntu/#upgrade-docker-ce

sudo apt-get remove docker docker-engine docker.io
sudo apt-get update

# Install packages to allow apt to use a repository over HTTPS
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update

# check
sudo apt-get install docker-ce


# install compose
sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

# add perms
sudo chmod +x /usr/local/bin/docker-compose

# add user to docker group
sudo usermod -aG docker $USER
+ выйти и войти обратно, применятся настройки групп