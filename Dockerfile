FROM python:3.5

ENV PYTHONUNBUFFERED 1
ENV APP tst
ENV APPDIR /app
ENV LOGDIR /var/log/app

RUN mkdir -p $APPDIR
RUN mkdir -p $LOGDIR

WORKDIR $APPDIR

# Use "bash" as replacement for "sh"
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN set -x && \
    apt-get update -y && \
    apt-get upgrade -y  && \
    apt-get install -y locales && \
    apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev libav-tools && \
    echo ru_RU.UTF-8 UTF-8 >> /etc/locale.gen && \
    locale-gen

# install wait-for-it
RUN wget https://github.com/vishnubob/wait-for-it/raw/b638c190275dc8f9abea5edd3faa69540e0395a9/wait-for-it.sh -O /wait-for-it.sh && \
    echo "d6bdd6de4669d72f5a04c34063d65c33b8a5450c  /wait-for-it.sh" | sha1sum -c - && \
    chmod +x /wait-for-it.sh

# install static
ADD requirements*.txt ${APPDIR}/
RUN pip install -r requirements.txt

ADD . ${APPDIR}/

#RUN ./manage.py collectstatic --noinput
EXPOSE 80
CMD /usr/local/bin/gunicorn --chdir $APPDIR --workers 10 --timeout 120 $APP.wsgi:application -b :80




